var WebSocketServer = require('websocket').server;
var http = require('http');
var webSocketsServerPort = 30000;
var val_x = 0;
var val_y = 0;
var trigger = "N";
var objid = "DISPLAY1";

var game_screen_width = 1080;
var game_screen_height = 1920;

var monitor_width = 2880;
var monitor_height = 1620;

var obj = new Object();
var connection = null;

// https://github.com/wilix-team/iohook/issues/124
const ioHook = require('iohook');


Number.prototype.map = function(in_min, in_max, out_min, out_max) {
    return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


function sendMsg() {
    obj.x = val_x;
    obj.y = val_y;
    obj.trigger = trigger;
    obj.objid = objid;
    var jsonString = JSON.stringify(obj);

    if (connection != null) {
        connection.send(jsonString);
    }
}

console.log("ioHook will be disabled when any keyboard input detected!")
ioHook.on("mousemove", event => {
    var px = event.x;
    var py = event.y;
    var mx = px.map(0, monitor_width, 0, game_screen_width);
    var my = py.map(0, monitor_height, 0, game_screen_height);
    var nx = mx / (game_screen_width / 2) - 1;
    var ny = my / (game_screen_height / 2) - 1;
    console.log("px: " + px + " / py: " + py);
    console.log("mx: " + mx + " / my: " + my);
    console.log("nx: " + nx + " / ny: " + ny);
    /* You get object like this
    {
       type: 'mousemove',
       x: 700,
       y: 400
     }
    */

    trigger = "N"
    val_x = nx;
    val_y = ny;

    //  val_x = px;
    //  val_y = py;
});

ioHook.on("mousedown", event => {
    console.log(event);
    //val_x = event.x;
    //val_y = event.y;

    console.log(event.button)
    if (event.button == 2) {
        trigger = "CANCEL"
    } else {
        trigger = "S"
    }
    sendMsg();

});

ioHook.on("mouseup", event => {
    console.log(event);
    //val_x = event.x;
    //val_y = event.y;

    // trigger = "C"
    // sendMsg();

});

ioHook.on("keydown", event => {
    console.log(event);
});


//Register and start hook 
ioHook.start();

/*
// Enable keystroke input
var stdin = process.stdin;

// without this, we would only get streams once enter is pressed
stdin.setRawMode( true );

// resume stdin in the parent process (node app won't quit all by itself
// unless an error or process.exit() happens)
stdin.resume();

// i don't want binary, do you?
stdin.setEncoding( 'utf8' );

// on any data into stdin
stdin.on( 'data', function( key ){
    // ctrl-c ( end of text )
    if ( key === '\u0003' ) {
        console.log("server down...");
        process.exit();
    }
    
    // else {
    //     if (key === 'd') {
    //         trigger = "N";
    //         val_x += 0.01;
    //         console.log("val_x: " + val_x);
    //     } else if (key === 'a') {
    //         trigger = "N";
    //         val_x -= 0.01;
    //         console.log("val_x: " + val_x);
    //     } else if (key === 's') {
    //         trigger = "N";
    //         val_y += 0.01;
    //         console.log("val_y: " + val_y);
    //     } else if (key === 'w') {
    //         trigger = "N";
    //         val_y -= 0.01;
    //         console.log("val_y: " + val_y);
    //     } else if (key === 'r') {
    //         trigger = "N";
    //         console.log("reset to 0, 0");
    //         val_x = 0; val_y = 0;
    //     } else if (key === 'o') {
    //         trigger = "RO";
    //         console.log("trigger: " + trigger);
    //     } else if (key === 'c') {
    //         trigger = "C";
    //         console.log("trigger: " + trigger);
    //     } else if (key === 'v') {
    //         trigger = "S";
    //         console.log("trigger: " + trigger);
    //     }

    // }


    // write the key to stdout all normal like
    // process.stdout.write( key );
});
*/



var server = http.createServer(function(request, response) {
    // process HTTP request. Since we're writing just WebSockets
    // server we don't have to implement anything.
});
server.listen(webSocketsServerPort, function() {

    console.log((new Date()) + " Server is listening on port " +
        webSocketsServerPort);

});

// create the server
wsServer = new WebSocketServer({
    httpServer: server
});

// WebSocket server
wsServer.on('request', function(request) {
    console.log((new Date()) + ' Connection from origin ' + request.origin + '.');
    console.log((new Date()) + ' Connection accepted.');
    connection = request.accept(null, request.origin);

    // console.log(jsonString);

    setInterval(function() {
        if (trigger === "N") {
            sendMsg();
        }
    }, 250);

    // This is the most important callback for us, we'll handle
    // all messages from users here.
    connection.on('message', function(message) {

        // if (message.type == 'utf8') { // accept only text
        //   console.log(message);
        // }

    });

    connection.on('close', function(connection) {
        console.log("ccccc");
        // close user connection
    });



});